FROM php:7.3-apache

RUN apt-get update -y; \
    apt-get upgrade -y;

RUN docker-php-ext-install mysqli

COPY src/ /var/www/html/

EXPOSE 80
