<html>
<head>
<title>Vibrato Test</title>
</head>
<body>
<h1> Popular Names</h1>
<?php
$mysql_pwd = $_ENV["MYSQL_ROOT_PASSWORD"];
$mysql_db = $_ENV["MYSQL_DATABASE"];

$conn = new mysqli("mysql", "root", $mysql_pwd, $mysql_db);
// Check connection
if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
}

$most = "SELECT given_name FROM names WHERE position = 1";
$least = "SELECT given_name FROM names WHERE position = 87";
$most_result = $conn->query($most);
$least_result = $conn->query($least);

if ($most_result->num_rows > 0) {
        // output data of each row
        while($row = $most_result->fetch_assoc()) {
                echo "Most Popular Names - Position 1: ".$row['given_name']."</br>";
        }
} else {
        echo "0 results";
}
if ($least_result->num_rows > 0) {
        // output data of each row
        while($row = $least_result->fetch_assoc()) {
                echo "Least Popular Names - Position 87: ".$row['given_name']."</br>";
        }
} else {
        echo "0 results";
}

$conn->close();