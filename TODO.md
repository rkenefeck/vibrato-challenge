Improvements:
In no particular order, removed these to get to an MVP quickly.

* [ ] PersistantVolume for MySQL (required for non-static data)
* [ ] Health check end points
* [ ] Logging
* [ ] Change data source
* [ ] CSS to make it look better
* [ ] User Interface for picking popularity
* [ ] Secrets instead of Env variables for passwords
* [ ] Not using Root Password
* [ ] Resolve AutoDevOps in Gitlab so it auto deploys while i'm working.

